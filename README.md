
# react express rethinkdb stack

dockerized stack for development


#### installation

- docker
```
- with docker compose
docker-compose up -d

- building the react image
docker build -t react -f Dockerfile.react .

- building the api image
docker build -t api -f api/Dockerfile.api api
```

- running things without docker

```
- react
yarn install
yarn start

localhost:3000


- api
yarn server
or
cd api && yarn server

localhost:8001


- rethinkdb
brew update && brew install rethinkdb

choose a binary for your needs
https://rethinkdb.com/docs/install/

localhost:8080
```



