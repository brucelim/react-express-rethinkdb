'use strict'

const router = require('express').Router();
const path = require('path');

require('fs').readdirSync(__dirname).forEach(file => {
    if (file === 'index.js') {
        return;
    }

    module.exports[path.basename(file, '.js')] = require(path.join(__dirname, file));
});

router.get('/', (request, response) => {
    response.send({ "__status": "running" });
});

module.exports['index'] = router;
