'use strict'

require('dotenv').config();

const express = require('express');
const compression = require('compression');
const cors = require('cors');
const helmet = require('helmet');
const body_parser = require('body-parser');
const cookie_parser = require( 'cookie-parser' );
const r = require('rethinkdb');

const app = express();

// configure
app.set('port', process.env.PORT || 8001);

app.use(compression());
app.use(body_parser.json());
app.use(body_parser.urlencoded({ extended: false }));

app.use(cookie_parser());
app.use(cors());
app.use(helmet());

const routes = require('./routes');
Object.keys(routes).map((key) => {
    app.use(routes[key]);
});

app.use((req, res, next) => {
    res.status(404).send({ "__status": "404, link to documentation" });;
});

app.use((req, res, next) => {
    res.status(505).send({ "__status": "505, link to documentation" });;
});

app.listen(app.get('port'), function() {
    console.log('Express server listening on port: ' + app.get('port'));
});
